import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.util.concurrent.TimeUnit;

public class HelloFromWebdriver {
    public static void main(String[] args) throws InterruptedException {

        ChromeOptions handlingssl = new ChromeOptions();
        handlingssl.setAcceptInsecureCerts(true);

        WebDriver driver = new ChromeDriver(handlingssl);
        driver.get("https://pastebin.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //write text
        WebElement searchInput = driver.findElement(By.id("postform-text"));
        searchInput.sendKeys("Hello from WebDriver");

        // Open the dropdown
        WebElement pasteExpirationDropdown = driver.findElement(By.xpath("//*[@id='w0']/div[5]/div[1]/div[4]/div/span"));
        pasteExpirationDropdown.click();

        // Select "10 Minutes" from the dropdown
        WebElement tenMinutesOption = driver.findElement(By.xpath("//li[text()='10 Minutes']"));
        tenMinutesOption.click();

        WebElement pasteName = driver.findElement(By.xpath("//*[@id='postform-name']"));
        pasteName.sendKeys("helloweb");

        WebElement create = driver.findElement(By.xpath("//*[@id='w0']/div[5]/div[1]/div[10]/button"));
        create.click();

        driver.quit();

    }
}
